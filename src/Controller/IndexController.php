<?php


namespace App\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class IndexController extends AbstractController
{
    /**
     * @Route("/", name="index")
     * @return Response
     */
    public function index()
    {
        $answers = [
            'Make sure your cat is sitting purrrfectly still 🤣',
            'Honestly, I like furry shoes better than MY cat',
            'Maybe... try saying the spell backwards?',
        ];
        return $this->render('index/index.html.twig', [
            'question' => 'Ara',
            'answers' => $answers,
        ]);
    }

    /**
     * @Route("/comments/{count}/vote/{direction}")
     */
    public function ezzhi()
    {
        return $this->json(['votes' => 21]);
    }
}